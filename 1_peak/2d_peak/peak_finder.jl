using JLD


function is_2dpeak(arr, i, j)
    max_i, max_j = size(arr)
    up_peak_cond = false
    down_peak_cond = false
    left_peak_cond = false
    right_peak_cond = false
    if i != 1
        down_peak_cond = (arr[i,j] >= arr[i-1,j])
    else
        down_peak_cond = true
    end
    if i != max_i
         up_peak_cond = arr[i,j] >= arr[i+1,j]
    else
        up_peak_cond = true
    end
    if j != 1
        left_peak_cond = arr[i,j] >= arr[i,j-1]
    else
        left_peak_cond = true
    end
    if j != max_i
        right_peak_cond = arr[i,j] >= arr[i,j+1]
    else
        right_peak_cond = true
    end
    return up_peak_cond & 
           down_peak_cond & 
           left_peak_cond & 
           right_peak_cond
end


function naive_2dpeak(arr)
    for i = 1:size(arr, 1)
        for j = 1:size(arr, 2)
            if is_2dpeak(arr, i, j)
                return [i,j]
            end
        end
    end
end


function find_2dpeak(arr)
    println("Call", size(arr))
    if size(arr, 2) == 1
        mxval, i = findmax(arr[:,1]; dims=1)
        # println("Return at condition ==1")
        return [i,1]
    end
    if size(arr, 2) == 2
        mxval_1, i_1 = findmax(arr[:,1]; dims=1)
        mxval_2, i_2 = findmax(arr[:,2]; dims=1)
        i = i_1
        j = 1
        if mxval_2 > mxval_1
            i = i_2
            j = 2
        end
        # println("Return at condition ==2")
        # println("i,j ", i, j)
        # println("mxval_1, mxval_2 ", mxval_1, mxval_2)
        return [i,j]
    end
    j = Integer(ceil(size(arr, 2) / 2))
    mxval, i = findmax(arr[:,j]; dims=1)
    i = i[1,1]
    # println("Current i: ",i)
    # println("Current j: ",j)
    # println("size(arr):", size(arr))
    gt_left = false
    gt_right = false
    # println("Here")
    if j > 1
        if arr[i,j]>=arr[i, j-1]  ## left
            gt_left = true
        end
    else
        gt_left = true
    end
    if j < size(arr,2)
        if arr[i,j]>=arr[i,j+1]
            gt_right = true
        end
    else
        gt_right = true
    end
    # print("Here")
    if gt_right && gt_left
        # println("Here and")
        return [i,j]
    elseif gt_right
        # println("Here gt right")
        best_j = find_2dpeak(@view arr[:, 1:j])[2]
        # println("best_j=",best_j)
        return [i,(j-1)+best_j]
    elseif gt_left
        # println("Here gt left")
        best_j = find_2dpeak(@view arr[:, j:end])[2]
        # println("best_j=",best_j)
        return [i,((j-1)+best_j)]
    end
end


function main()
    arr = [1 2 3; 4 5 6; 7 8 9]
    println(arr)
    println(is_2dpeak(arr, 1, 2)) # true
    println(is_2dpeak(arr, 2, 2)) # true
    println(is_2dpeak(arr, 3, 2)) # true
    println(is_2dpeak(arr, 1, 1)) # false
    println(is_2dpeak(arr, 3, 3)) # false
    for i = 1:size(arr,1)
        for j = 1:size(arr,2)
            println("ij=","(",i,",",j,")", " is_2dpeak:", is_2dpeak(arr, i, j))
        end
    end
    println(naive_2dpeak(arr))
    potential_peak_i, potential_peak_j = find_2dpeak(arr)
    println("fast:", potential_peak_i," ", potential_peak_j)
    println(is_2dpeak(arr, potential_peak_i, potential_peak_j))

    println("Large array test")
    a = reshape(cumsum(ones(400*400)), 400,400)

    potential_peak_i, potential_peak_j = find_2dpeak(a)
    println("fast:", potential_peak_i," ", potential_peak_j)
    println(is_2dpeak(a, potential_peak_i, potential_peak_j))
    println(potential_peak_i, " ", potential_peak_j)


end

main()