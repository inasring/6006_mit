using JLD
N = 100000000
# large_arr = rand(Float32, N)
# large_arr = vcat(collect(1:303000), collect(1:100001))
large_arr = vcat(collect(1:N), -collect(-N:-1), collect(1:N))
save("data.jld", "data", large_arr)
