using JLD

function is_peak(arr, i)
    if size(arr, 1)==1
        return true
    end

    # assume array length > 1
    # First element
    if i == 1 && arr[i]>arr[i+1]
            return true
    end
    # Last element
    if i == size(arr, 1) && arr[i]>arr[i-1]
            return true
    end
    # Element anywhere in the middle
    if arr[i]>=arr[i+1] && arr[i]>=arr[i-1]
        return true
    end
    return false
end


function naive_find_peak(arr)
    for i = 1:size(arr, 1)
        if is_peak(arr, i)
            return i
        end
    end
end

function find_peak(;arr)
    look_position = Int(ceil(size(arr,1)//2))
    # print("\nlook_position:",look_position)
    # print(" value:",arr[look_position])
    # print(" array_size:",size(arr, 1))
    if is_peak(arr, look_position)
        return look_position
    end
    if (look_position==1) && (arr[look_position]>arr[look_position+1])
        return look_position
    end
    if (look_position==size(arr,1)) && (arr[look_position]>arr[look_position-1])
        return look_position
    end
    if size(arr, 1)==2
        if arr[1]>arr[2]
            return 1
        else
            return 2
        end
    end
    if  arr[look_position]>arr[look_position-1]
        return (look_position-1) + find_peak(arr=@view arr[look_position:end])
    else
        return find_peak(arr=@view arr[1:look_position])
    end
end


function main()
    arr = [1, 2, 4, 5, 3, 8, 7]
    @assert(!is_peak(arr, 3))
    @assert(is_peak(arr, 4))

    arr2 = [10, 2, 4, 5, 3, 8, 10]
    @assert(is_peak(arr2, 1))
    @assert(is_peak(arr2, 7))
    @assert(!is_peak(arr2, 2))
    @assert(!is_peak(arr2, 3))

    naive_peak = naive_find_peak(arr)
    println("value:", arr[naive_peak], " ", "position:", naive_peak)
    @assert(is_peak(arr, naive_peak))

    ret = find_peak(arr=arr)
    println(ret)
    @assert(is_peak(arr, ret))
    println("Large Array:")
    @time large_arr = load("data.jld")["data"]
    print("Naive:\n")
    @time ret = naive_find_peak(large_arr)
    print("Peak find:\n")
    @time ret = find_peak(arr=large_arr)
    println("large_array ",
            "position: ", ret,
            " value: ", large_arr[ret])
    println(is_peak(large_arr, ret))
end


main()
